<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('/user','UsersController@getCurrent');

Route::get('/bookings', 'BookingsController@index');
Route::get('/bookings/next', 'BookingsController@getNextBookings');
Route::get('/bookings/{id}', 'BookingsController@get');
Route::post('/bookings', 'BookingsController@store');
Route::delete('/bookings/{id}', 'BookingsController@destroy');

Route::get('/rooms', 'RoomsController@index');
Route::get('/rooms/{id}', 'RoomsController@get');
Route::post('/rooms', 'RoomsController@store');
Route::delete('/rooms/{id}', 'RoomsController@destroy');

Route::get('/roomCategories', 'RoomCategoriesController@index');
Route::get('/roomCategories/{id}', 'RoomCategoriesController@get');
Route::post('/roomCategories', 'RoomCategoriesController@store');
Route::delete('/roomCategories/{id}', 'RoomCategoriesController@destroy');

Route::get('/clients', 'ClientsController@index');
Route::get('/clients/{id}', 'ClientsController@get');
Route::post('/clients', 'ClientsController@store');
Route::delete('/clients/{id}', 'ClientsController@destroy');
