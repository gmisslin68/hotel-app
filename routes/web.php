<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
// Route::get('/', 'HomeController@index')->name('home');

Route::get('users/logout', 'UsersController@logout');

Route::get('users/getCurrent', 'UsersController@getCurrent');

//Route pour télécharger/afficher des fichiers du dossier Public
Route::get('public/img/{filename}', function ($filename)
{
    $path = public_path('img/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});


//Enlever le '#' dans les routes
Route::get('/{vue?}', 'HomeController@index')->where('vue', '[\/\w\.-]*');
