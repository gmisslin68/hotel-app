<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/928eb9b174.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
      <el-container>
        <el-aside width="200px" style="background-color: #FFFFFF">
          <el-menu
            default-active="1"
            router="router"
            class="el-menu-vertical-demo"
            style="height:100vh">
            <el-menu-item index="1" :route="{ name : 'home'}">
              <i class="fas fa-desktop"></i>
              <span>Accueil</span>
            </el-menu-item>
            <el-menu-item index="2" :route="{ name : 'bookings'}">
              <i class="fas fa-passport"></i>
              <span>Mes Réservations</span>
            </el-menu-item>
            <el-menu-item index="3" :route="{ name : 'hotel'}">
              <i class="fas fa-hotel"></i>
              <span>Mon Hôtel</span>
            </el-menu-item>
          </el-menu>
        </el-aside>

        <el-container>
          <el-header style="text-align: right; color:black; background-color: #FFFFFF; font-size: 12px " >
            <el-row type="flex" class="row-bg" justify="space-between" >
              <div class="">
                <el-page-header title="Retour" @back="goBack" :content="the_name_of_page()" class="pt-3">
                </el-page-header>
              </div>



              <el-menu default-active="1" mode="horizontal">
                <el-submenu index="1">
                  <template slot="title">@{{currentUser.firstname}}</template>
                  <el-menu-item index="2-1">Profil</el-menu-item>
                  <el-menu-item index="2-2" @click="logout">Se déconnecter</el-menu-item>
                </el-submenu>
              </el-menu>
            </el-row>
          </el-header>


          <el-main>
            @yield('content')
          </el-main>
      </el-container>
    </div>
</body>
</html>
