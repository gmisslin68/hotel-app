/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');
// window.VueRouter = require('vue-router');
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import router from './routes';

import Vuex from 'vuex'
Vue.use(Vuex);

import ElementUI from 'element-ui';
import moment from 'moment';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/fr'

Vue.use(ElementUI, { locale })


var BASE_URL = window.location.origin+ "/";
var IMG_URL = BASE_URL + "public/img/";

window.BASE_URL = BASE_URL;
window.IMG_URL = IMG_URL;

import VuexStore from './store';
export default VuexStore;

window.VuexStore = VuexStore;


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

 //*********** Composants : ***********//

Vue.component('room-form-crud', require('./components/hotel/Rooms/formCrud.vue').default);
Vue.component('client-form-crud', require('./components/hotel/Clients/formCrud.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    router,
    VuexStore,
    // vuetify: new Vuetify(),
    el: '#app',
    created:function(){
      VuexStore.commit('getCurrentUser');
    },
    computed: {
      currentUser() {
          return VuexStore.state.currentUser
      },
    },
    methods:{
      logout:function(){
        axios.get(BASE_URL+'users/logout')
        .then((response) =>{
          document.location.reload(true);
        })
        .catch(error => {
          this.loading=false;
          this.$alert('Erreur', 'Warning', {
              confirmButtonText: 'OK',
              callback: action => {

              }
          });
        });
      },
      goBack:function(){
        history.go(-1)
      },
      the_name_of_page:function(){
        return this.$route.meta.title
      }
    }
});
