import VueRouter from 'vue-router';


let routes = [
	{
		path: '/',
		name: 'home',
		meta : {
			title:'Accueil',
		},
		component: require('./components/Home.vue').default
	},
	{
		path: '/home',
		name: 'home2',
		meta : {
			title:'Accueil',
		},
		component: require('./components/Home.vue').default
	},
	{
		path: '/bookings',
		name: 'bookings',
		meta : {
			title:'Vos Réservations',
		},
		component: require('./components/Bookings/index.vue').default
	},
	{
		path: '/bookings/:tokenBooking',
		name: 'crud-booking',
		meta : {
			title:'Modification/Création de la réservation',
		},
		component: require('./components/Bookings/booking.vue').default
	},
	{
		path: '/hotel',
		name: 'hotel',
		meta : {
			title:'Votre Hôtel',
		},
		component: require('./components/Hotel/index.vue').default
	},
	{
		path: '/rooms',
		name: 'rooms',
		meta : {
			title:'Vos Chambres',
		},
		component: require('./components/Hotel/Rooms/index.vue').default
	},
	{
		path: '/clients',
		name: 'clients',
		meta : {
			title:'Vos Clients',
		},
		component: require('./components/Hotel/Clients/index.vue').default
	},
	{
		path: '/roomsCategories',
		name: 'roomsCategories',
		meta : {
			title:'Vos Catégories De Chambre',
		},
		component: require('./components/Hotel/RoomCategories/index.vue').default
	}
]

export default new VueRouter({
	routes,
	mode: 'history',
	hashbang: false,
	linkActiveClass : "active"
})
