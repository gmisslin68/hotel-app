import Vuex from 'vuex';
import Vue from 'vue';
import router from './routes.js';
Vue.use(Vuex);


export default new Vuex.Store({
   	state: {
	    currentUser:[]
	},
	mutations: {
		updateCurrentUser(state,user)
		{
			state.currentUser = user;
		},
    getCurrentUser(state)
    {
      axios.get(BASE_URL+'users/getCurrent')
      .then((response) =>{
        console.log(response)
        if(response.data)
        {
          state.currentUser = response.data;
        }
        else{
          if(window.location.pathname != "/login" && window.location.pathname != "//login" && window.location.pathname != "/register")
          {
              window.location.href = BASE_URL+'/login';
          }
        }
      })
      .catch(function (error) {
          if(window.location.pathname != "/login" && window.location.pathname != "/home" && window.location.pathname != "/register")
          {
              window.location.href = BASE_URL+'/login';
          }
      });
    }
  },
  getters:{
  		getUser: state => state.currentUser
  }
})
