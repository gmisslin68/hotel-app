<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Room;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::orderBy('created_at', 'desc')->with('roomCategory')->get();
        return $rooms;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $roomData = $request->all()['room'];
      $id = null;
      if(array_key_exists('id',$roomData))
      {
        $id = $roomData['id'];
      }

      if($id)
      {
        unset($roomData['id']);
        unset($roomData['created_at']);
        $roomData['updated_at'] = date('Y-m-d H:i:s');
        //SAVE DES INFOS GENERALES DU BOOKING
        $save = Room::find($id)->update($roomData);
        if(!$save){
          return false;
        }
        else{
          $room = Room::find($id);
        }
      }
      else{
        unset($roomData['id']);

        $roomData['category_id'] = $roomData['category_id'];
        $roomData['number'] = $roomData['number'];
        $roomData['price'] = $roomData['price'];

        $roomData['created_at'] = date('Y-m-d H:i:s');
        $roomData['updated_at'] = date('Y-m-d H:i:s');

        //SAVE DES INFOS GENERALES DE LA CHAMBRE
        $room = Room::create($roomData);
        $id = $room['id'];
        if(!$room){
          return false;
        }
      }

      return $room;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Room::find($id)->delete();
    }
}
