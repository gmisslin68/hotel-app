<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{


  public function getCurrent()
  {
    $user = Auth::user();
    return $user;
  }

  public function logout()
  {
      Auth::logout();
      return 'true';
  }
}
