<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomCategorie;
use App\Room;

class RoomCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roomCategories = RoomCategorie::orderBy('created_at', 'desc')->with('rooms')->get();
        return $roomCategories;
    }
}
