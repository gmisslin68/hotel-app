<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Client;
use Response;
use Illuminate\Support\Str;
class BookingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::orderBy('created_at', 'desc')->with('clients','order_client','room.roomCategory')->get();
        return $bookings;
    }


    /**
     * Display a listing of the next bookings.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNextBookings()
    {
        $bookings = Booking::where('start_date','>=',date('Y-m-d'))->orderBy('created_at', 'desc')->with('clients','order_client','room.roomCategory')->get();
        return $bookings;
    }

    /**
     * Get the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bookingData = $request->all()['booking'];
        $clients = $bookingData['clients'];
        $orderClient = $bookingData['order_client'];
        $id = null;
        if(array_key_exists('id',$bookingData))
        {
          $id = $bookingData['id'];
        }

        if($id)
        {
            unset($bookingData['id']);
            unset($bookingData['room']);
            unset($bookingData['clients']);
            unset($bookingData['token']);
            unset($bookingData['created_at']);
            $bookingData['updated_at'] = date('Y-m-d H:i:s');
            //SAVE DES INFOS GENERALES DU BOOKING
            $booking = Booking::find($id)->update($bookingData);
            if($booking)
            {
                //SAVE DU CLIENT PRINCIPAL
                $saveOrderClient = Client::find($orderClient['id'])->update($orderClient);
                $idOrderClient = $orderClient['id'];
            }
            else{
              return false;
            }
        }
        else{

          //SAVE DU CLIENT PRINCIPAL
          $saveOrderClient = Client::create($orderClient);
          if($saveOrderClient)
          {
            $idOrderClient = $saveOrderClient['id'];
            unset($bookingData['id']);
            unset($bookingData['room']);
            unset($bookingData['clients']);

            //Loop to check if the token is free
            $tokenFree = 0;
            while($tokenFree < 1)
            {
                $token = bin2hex(random_bytes('15'));
                if(Booking::GetByToken($token)->count() == 0)
                {
                    $tokenFree = 1;
                }
            }

            $bookingData['client_id'] = $saveOrderClient['id'];
            $bookingData['token'] = $token;
            $bookingData['reference'] = strtoupper( date('Y').Str::random('3') );

            $bookingData['created_at'] = date('Y-m-d H:i:s');
            $bookingData['updated_at'] = date('Y-m-d H:i:s');
            //SAVE DES INFOS GENERALES DU BOOKING
            $booking = Booking::create($bookingData);
            $id = $booking['id'];
            if(!$booking){
              return false;
            }
          }
        }


        $booking = Booking::find($id);


        //SAVE DES CLIENTS SECONDAIRES
        //On disocie tous les clients
        $booking->clients()->detach();
        //Et on associe ceux du formulaire
        foreach($clients as $client)
        {
          $idClient = null;
          if(($client['id'] && $orderClient['id'] != $client['id']) || !$client['id'] )
          {
            if($client['id'])
            {
              $idClient = $client['id'];
              $save = Client::find($client['id'])->update($client);
            }
            else{
              $save = Client::create($client);
              $idClient = $save['id'];
            }

            if($save)
            {
              $booking->clients()->attach($idClient);
            }
          }
        }

        Booking::find($id)->clients()->attach($idOrderClient);

        return $booking;
    }

    /**
     * Date of the specified resource.
     *
     * @param  int  $tokenBooking
     * @return \Illuminate\Http\Response
     */
    public function get($tokenBooking)
    {
        $booking = Booking::getByToken($tokenBooking)->with('clients','order_client','room.roomCategory')->first();
        return array('booking' => $booking, 'lastReference' => 'test');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Booking::find($id)->delete();
    }
}
