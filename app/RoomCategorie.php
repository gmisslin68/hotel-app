<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomCategorie extends Model
{

    protected $table = 'room_categories';
    public function rooms()
    {
       return $this->hasMany('App\Room','category_id');
    }
}
