<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function roomCategory()
    {
       return $this->belongsTo('App\RoomCategorie','category_id');
    }

    protected $fillable = ['number','price','category_id','created_at','updated_at'];
}
