<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function order_client()
    {
       return $this->belongsTo('App\Client','client_id');
    }

    public function clients()
    {
       return $this->belongsToMany('App\Client');
    }

    public function room()
    {
       return $this->belongsTo('App\Room');
    }

    public function ScopeGetByToken($query,$token)
    {
        return $query->where('token', $token);
    }

    protected $fillable = ['reference','token','client_id','room_id','start_date','end_date','updated_at'];
}
