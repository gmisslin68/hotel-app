<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function bookings()
    {
       return $this->belongsToMany('App\Booking');
    }


    protected $fillable = ['name','firstname','birthday','passeport','email','phone','updated_at'];
}
