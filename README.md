# Hotel-App

Petite application de gestion d'hôtel.
Gestion des chambres, réservations, clients,...

Installation : 

*  npm install
*  composer install
*  créer un fichier .env avec votre environnement 
*  php artisan migrate:install
*  php artisan serve